<?php

include '../operacoes/listamaiornum.php';
include '../operacoes/positivo.php';
include '../operacoes/raizquadrada.php';

//6) Retorne o maior número de números negativos, some mais 10 até o número ficar positivo, valide se o número é positivo, caso seja, imprima a raiz quadrada desse número  convertido em inteiro.

// Declaração de variáveis
$array = array(-2, -3, -1, -6, -7, -60, -98, -74, -15, -56, -95, -32, -114, -458, -124, -5);

// Método para listar o maior número do array
$result = NumMaior($array);

// Loop para validar se o número é positivo, se não, soma mais 10 até que ele se torne positivo
while ($result != Positivo($result)) {
    $result = $result + 10;

}
// Validação do resultado
if (isset($result) ? $result : 0) {

    //Imprime e calcula a raiz quadrada da parte inteira do núemro
    echo RaizQ(intval($result));
} else {
    echo "Error";
}

?>
