<?php

include '../operacoes/subtracao.php';
include '../operacoes/multiplicacao.php';

//3) Multiplique duas vezes o número (deverá passar um ponto flutuante), converta o resultado para inteiro e substraia pelo ponto flutuante.

// Declaração de variáveis
$v1 = 45.333;
$v2 = 2;


// Método para efetuar a multiplicação
$result = Multiplicar($v1, $v2);

// Método nativo do php para transformar um valor em inteiro
$result = intval($result);

// Método para efetuar a subtração e imprimir o resultado
echo $result = Subtrair($result, $v1), "\n";

?>