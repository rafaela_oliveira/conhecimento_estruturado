<?php

include '../operacoes/multiplicacao.php';
include '../operacoes/restodadivisao.php';
include '../operacoes/divisao.php';


// 10) Multiple o número a quantidade de vezes que ele representa, depois divida por ele mesmo e retorno o resto da divisão.

// Declaração de variáveis
$v1 = 5;

// Método para efetuar a multiplicação
$result = Multiplicar($v1, pow($v1, $v1));

// Método para efetuar a divisão
$result = Dividir($result, $result);

// Função para retornar o resto da divisão e exibir o resultado
echo $result = Resto($result, $result);
?>


