<?php

include '../operacoes/multiplicacao.php';
include '../operacoes/restodadivisao.php';
include '../operacoes/divisao.php';
include '../operacoes/valordepi.php';
include '../operacoes/listamenornum.php';
include '../operacoes/listamaiornum.php';


//9) Pegue o resto de uma divisão, multiplique por PI, retorne o menor e o maior número dessa lista e some-os.


// Declaração de variáveis
$v1 = 56;
$v2 = 45;

// Função para retornar o resto da divisão
$result = Resto($v1, $v2);

// Método para efetuar a multiplicação
$result = Multiplicar($result, ValordePi());

// Função para transformar um inteiro em uma string
$result = (string)$result;

// Função para tirar o ponto da string
$arr1 = str_replace('.', '', $result);

//Função para transformar uma string em um array
$arr2 = str_split($arr1);

//Função para retornar o menor valor
$menor = NumMenor($arr2);

//Função para retornar o maior valor
$maior = NumMaior($arr2);


//Soma do maior e menor valor da lista
$result = $menor + $maior;


echo 'Resultado: ' . $result;

?>

