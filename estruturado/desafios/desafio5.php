<?php

include '../operacoes/multiplicacao.php';
include '../operacoes/raizquadrada.php';
include '../operacoes/valordepi.php';

//5) Faça a raiz quadrada de 36, multiplicado pelo valor de PI, retorne apenas os últimos 2 números do resultado da multiplicação.

// Declaração de variáveis
$v1 = 36;
$v2 = Pi();

// Método para calcular a raiz quadrada
RaizQ($v1);

// Método para efetuar a multiplicação
$result = Multiplicar($v1, $v2);

// Função para formatar o valor em 2 casas decimais
$result = number_format($result, 2, ",", "");

// Função para substituir a vírugla por ponto
$result = str_replace(',', '.', $result);

// Código inventado(rs) para pegar somente o valor após a vírgula
$result = ($result - (int)$result) * pow(10, 2);

//Código para imprimir e formatar novamente o valor recebido
echo $result = number_format($result, 0, ",", "")," \n";


