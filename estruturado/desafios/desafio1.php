<?php

include '../operacoes/adicao.php';
include '../operacoes/divisao.php';

// Declaração de variáveis
$v1 = 56;
$v2 = 45;

// Método para efetuar a soma
$result = Adicao($v1, $v2);

// Método para efetuar a divisão
$result = Dividir($result, $v2);

// Função para retornar o valor inteiro da divisão
$result = intval($result);

// Validação do resultado
if (isset($result) ? $result : 0) {
    echo($result);
} else {
    echo "Error";
}

?>


