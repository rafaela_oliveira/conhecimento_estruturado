<?php

include '../operacoes/subtracao.php';
include '../operacoes/divisao.php';

//2) Divida dois números, subtraia o resultado da divisão pelo primeiro número.

// Declaração de variáveis
$v1 = 20;
$v2 = 10;

// Declaração e validação de variáveis
$v1 = isset($v1) ? $v1 : 0;
$v2 = isset($v2) ? $v2 : 0;


// Método para efetuar a divisão
$result = Dividir($v1, $v2);

// Método para efetuar a subtração e imprimir
echo $result = Subtrair($result, $v1), "\n";

?>
