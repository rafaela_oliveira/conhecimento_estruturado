<?php

include '../operacoes/multiplicacao.php';
include '../operacoes/raizquadrada.php';
include '../operacoes/divisao.php';

//4) Divida dois números e extraia apenas a fração, transforme cada número fracionado em uma lista, divida um pelo outro (loop) e some todos os restos da divisao.

// Declaração de variáveis
$v1 = 35;
$v2 = 6;

// Método para efetuar a divisão
$result = Dividir($v1, $v2);

// Código inventado(rs) para pegar somente o valor após a vírgula
$result = ($result - (int)$result) * pow(10, 2);

// Função para tirar o ponto
$result = str_replace('.', '', $result);

// Função para transformar um inteiro em uma string
$result = (string)$result;

//Função para transformar uma string em um array
$arr1 = str_split($result);

//Função para dividir e depois somar os elementos do array. Obs: por eles mesmos e no final imprimi o resultado;
foreach ($arr1 as &$value) {
    $value = $value / $value;
    $value = $value + $value;
    echo $value, "\n";
}

?>