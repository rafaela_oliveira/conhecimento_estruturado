<?php

#Só funciona se tirar a função
function Listanumimpares($veta)
{
    $RAND_MAX = 32768; //QTDE máximos de elementos aleatórios
    $TAMANHO = 6; //QTDE máximos de elementos do vetor

//Armazena números aletórios no vetor
    for ($i = 0; $i < $TAMANHO; $i++) {

        $N = mt_rand(0, $RAND_MAX); //leitura dos números aleatórios

        $veta[$i] = $N; //armazena números aleatórios

    }//fecha for

    $conta_impar = 0; //inicia contador de numeros impares

//Conta os números impares
    for ($i = 0; $i < $TAMANHO; $i++) {

        switch ($veta[$i]) {

            case ($veta[$i] % 2 != 0): {

                $conta_impar++;
                break;

            }//fecha case

        }//fecha switch

    }//fecha for


//Lista na tela os números ímpares
    for ($i = 0; $i < $TAMANHO; $i++) {

        if ($veta[$i] % 2 != 0) {

            echo "Números impares encontrados no vetor: $veta[$i]<br>";

        }//fecha if

    }//fecha for

}

?>

