<?php

#Função para lisstar os  números pares de um array aleatório
    function ListaNumPares()
    {

        $RAND_MAX = 32768; //QTDE máximos de elementos aleatórios
        $TAMANHO = 6; //QTDE máximos de elementos do vetor

//Armazena números aletórios no vetor
        for ($i = 0; $i < $TAMANHO; $i++) {

            $N = mt_rand(0, $RAND_MAX); //leitura dos números aleatórios

            $veta[$i] = $N; //armazena números aleatórios

        }//fecha for


        $conta_par = 0; //inicia contador de numeros pares


//Conta os números pares e impares
        for ($i = 0; $i < $TAMANHO; $i++) {

            switch ($veta[$i]) {

                case ($veta[$i] % 2 == 0): {

                    $conta_par++;
                    break;

                }//fecha case

            }//fecha switch

        }//fecha for


//Lista na tela os números pares
        for ($i = 0; $i < $TAMANHO; $i++) {

            if ($veta[$i] % 2 == 0) {

                echo "Lista dos números pares encontrados no vetor: $veta[$i]<br>";

            }//fecha if

        }//fecha for


    }
?>