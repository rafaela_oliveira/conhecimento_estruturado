<?php

# Função "Regra" para executar o cálculo dos valores (v1, v2 e v3)
function Regra($v1, $v2, $v3)
{
    # Armazena o valor de [v1] na variável $c1
    $c1 = $v1;

    # Armazena a multiplicação de [v2 + v3] na variável $c2
    $c2 = $v2 * $v3;

    # Armazena a divisão de [c2 e c3] na variável $total
    $total = $c2 / $c1;

    # Exibe a variável $total com os valores já calculados
    return $total;

}

?>

